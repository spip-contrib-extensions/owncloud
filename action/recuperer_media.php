<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('owncloud_fonctions');
include_spip('inc/distant');

/**
 * Récupérer les médias dans un fichier json à la racine de tmp/
 * 
 * @param string $arg l'URL cible
 * @return string
 */
function action_recuperer_media_dist() {
spip_log( "action_recuperer_media_dist()", 'owncloud.' . _LOG_DEBUG);
	include_spip('inc/config');
	$config = lire_config('owncloud');
	/* clear dossier de téléchargement local local */
	rrmdir(_DIR_TMP . 'upload/nextcloud' );
    mkdir(_DIR_TMP . 'upload/nextcloud' );

	$url = construire_url();

	$code = '' ;

	include_spip('lib/SabreDAV/vendor/autoload');

	$settings = array(
		'baseUri' => $url['url'],
		'userName' => $config['login'],
		'password' => $config['password']
	);
spip_log( $settings['baseUri'], 'owncloud.' . _LOG_DEBUG);	

	try {
		$client = new Sabre\DAV\Client($settings);
		$liste = $client->propfind($settings['baseUri'], array('{DAV:}resourcetype', '{DAV:}getcontentlength', '{DAV:}getlastmodified', '{DAV:}getcontenttype'), 1);
	} catch (Exception $e) {
		$code = $e->getMessage();
		if ($code) {
			$erreur = 'oui';
		}
		return $erreur;
	}

	if (!in_array($code, array('401', '404', '405', '501')) || !$code) {
		$fichiers=array();

		foreach ($liste as $cle => $valeur) {	
			$document = $url['url_courte'].$cle;

//spip_log( "lastmodified  = " . $valeur['{DAV:}getlastmodified'] . " contentlength = " . $valeur['{DAV:}getcontentlength'] . " contenttype = " . $valeur['{DAV:}getcontenttype'], 'owncloud.' . _LOG_DEBUG);

			if ( !empty($valeur['{DAV:}getcontenttype']) ) {
				
			$md5 = md5(urldecode(basename($document)).$valeur['{DAV:}getlastmodified'].$valeur['{DAV:}getcontentlength']);

			$body = recuperer_infos_distantes_curl($document);
			
			if (preg_match('/(image|application|text)/', $valeur['{DAV:}getcontenttype']) && isset($valeur['{DAV:}getcontentlength'])) {
// moi				$document = securise_identifiants($document);

				array_push(
								$fichiers,
								array(
									'nom' => $cle,
									'document' => $document,
									'md5' => $md5,
									'body' => array(
										'titre' => urldecode(basename($cle)),
										'extension' => $body['extension'],
										'taille' => $valeur['{DAV:}getcontentlength'],
										'fichier' => $body['fichier'],
										'mime_type' => $valeur['{DAV:}getcontenttype']),
									'getlastmodified' => $valeur['{DAV:}getlastmodified'])
				);
			}

		}
		$json = json_encode($fichiers, true);
		include_spip('inc/flock');
		ecrire_fichier(_DIR_TMP . 'owncloud.json', $json);
}
		return $fichiers;

	}

}
