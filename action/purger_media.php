<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('owncloud_fonctions');

/**
 * Purger les médias dans SPIP
 * 
 * @param string $arg l'URL cible
 * @return string
 */
function action_purger_media_dist() {
spip_log( "action_purger_media_dist", 'owncloud.' . _LOG_INFO);
	$purger = purger_media_spip();

	return $purger;
}
