SPIP - owncloud
===============

Ce plugin permet d'importer avec webdav des m�dias dans SPIP depuis owncloud/nextcloud.

Vous pouvez r�cup�rer la liste des fichiers depuis votre owncloud en peuplant un fichier jsoa la racine de tmp/, dans ce fichier json on retrouve la liste des fichiers pr�sents sur le owncloud dans le r�pertoire que vous avez renseignez dans la configuration. Ensuite, vous pouvez importer vos fichiers dans SPIP un par un.

Vous pouvez activer la syncho sur un r�pertoire de owncloud pour importer automatiquement les documents dans SPIP. On stock le md5 dans une base pour ne pas ins�re � nouveau le document. La synchro vous permet d'importer automatiquement beaucoup de document dans SPIP. 

Vous pouvez �galement purger vos documents d�j� import� dans SPIP (remise � z�ro)

** Vous ne pouvez plus effacer ** les documents distants de votre Owncloud apr�s les avoir importer dans SPIP.

Mais une option "effacer documents locaux" vous permet :
- non-coch�e, les nouveaux fichiers sur votre Cloud sont ajout�s aux documents pr�c�dement obtenus (ou mis � jour) dans vos documents Spip 
- coch�e, les nouveaux fichiers de votre Cloud sont biensur ajout�s � vos documents, mais les fichiers qui ont �t� supprim�s dans le Cloud seront aussi supprim�s le lendemain des documents Spip.


# Changelog

### Version 1.x.x

### Version 1.x.x

### version 1.3.3 (24/02/2022)
- probl�me encodage fichier README

### version 1.3.2 (24/02/2022)
- r�tablissement du document README
### Version 1.3.1 (23/02/2022)

### Version 1.3.0 (23/02/2022)

- informations fonctionnalit�s, mis a jour documentation 
- fonctionnement SPIP 4.0 OK

### Version 1.2.1 (19/02/2022)

- correction explication effacement_distants par effacement_locaux
- ajout fonction "regrouper les documents dans un album"

### Version 1.2.0 (16/02/2022)

- reprise fonctionnelle
- supression des acc�s  https Basic 0authentification
- les fichiers du Cloud sont copi�s dans un r�pertoire temporaire lors de la "r�cuperation des documents"
- puis ajouter aux documents "local" Spip
- la synchronisation effectue ces memes actions toutes les 6h 
- les fichiers disparus du Cloud sont aussi supprim�s des documents Spip si l'option "effacement des documents locaux"
- l'option "effacement des documents locaux" ne fait aucune intervention cot� Cloud, on ne touche pas au Cloud

** a venir **
l'ajout des documents dans un album sp�cifique


### Version 1.1.0 (26/01/2022)

- validation spip 3.2.X
- accepte tous les types de document valide (image, pdf, txt, opendocument, ...)
- attention n�cessite une correction dans le fichier ecrire/inc/distant.php
  remplacer apr�s la ligne 169 if (isset($parsed_url['user']) or isset($parsed_url['pass'])) {
       la ligne 170 " return false; " par " if (!protocole_verifier($url,'https')) return false;  "

### Version 1.0.9 (01/02/2017)

- Correction bug formulaire de configuration
- P�touille de #r102585

### Version 1.0.8 (31/01/2017)

- Ajout d'un formulaire de configuration avec traitement des donn�es plus logique
- Ajout de l'importation de tous les m�dias en un clique
- Mise � jour de la librairie SabreDav
- Traitement des erreurs de connexion

### Version 1.0.7 (12/10/2016)

- Ajout de la fonction curl pour acc�l�rer la r�cup�ration des fichiers distants et accessoirement passer https

### Version 1.0.6 (28/09/2016)

- Modification de d�claration des champs de id_owncloud dans les tables, suppression de unsigned pour la compat avec sqlite

### Version 1.0.5 (01/06/2016)

- Ajout de la doc vers contrib dans le paquet.xml
- Suppression du md5 dans la table spip_ownclouds lors de la suppression 
  d'un document ins�r� dans SPIP.
- Am�lioration des erreurs lors de la r�cup�ration des fichiers
- R�cup�rer seulement des fichiers et non les r�pertoires

### Version 1.0.4 (17/05/2016)

- On s�curise les URL pour ne pas voir appara�tre le mot de passe de Owncloud

### Version 1.0.3 (15/05/2016)

- G�rer les sous-r�pertoires
- Les champs obligatoires dans le formulaire de configuration fonctionnels
- Ajout d'un lien pour acc�der directement au document quand il est import� dans SPIP.

### Version 1.0.2 (14/05/2016)

- Ajout d'un test de connexion � webdav sur la liste
- Ajoute la possibilit� de purger les documents importer dans SPIP
- Ajoute la possibilit� d'activer ou desactiver la syncho vers owncloud
- Ajoute la possibilit� d'effacer ou non les documents distants du owncloud avec webdav

### Version 1.0.1 (13/05/2016)

- Detecter avec un md5 si le document est deja ins�r� dans SPIP
- Ajout un crontab qui aspire les m�dias automatiquement et les importe dans SPIP

###�Version 1.0.0 (11/05/2016)

- Configurer le plugins pour se connecter � owncloud
- R�cup�rer les m�dias avec webdav du owncloud
- Ajout d'un formulaire pour peupler le fichier json
- Importer les m�dias dans la base de SPIP et le syst�me de fichiers
- Concater l'URL et la taille pour faire un MD5 pour identifier les fichiers
- G�rer les erreurs proprement lorsque l'authentification �choue


